# dxvk-tools  
I got annoyed at manually editing prefixes for vulkan and dxvk, so I made a script to do it for me.  I also made a quick script for compiling dxvk.  

# installvulkan.sh - use this to install vulkan and optionally dxvk to a wine prefix.  
Note: It is -not- required to compile dxvk using dxvkbuild.sh. This script will automatically download the latest build from https://haagch.frickel.club/files/dxvk/latest/ if win64 or win32 folders are not detected.

Usage:  
To setup vulkan in either wine-vulkan or latest wine/wine-staging (build 2/28/18 or higher):  

WINEARCH=the architecture you want for your prefix (win32/win64)  
WINEPREFIX=the folder for your wine prefix  

Options:  
DXVK=1 set this if you want to install dxvk for experimental dx11->vulkan rendering  
VULKANSDK=1 set this if you want to install the vulkan sdk  

First chmod the script:
```
chmod a+x installvulkan.sh
```
Then use like so: 

Windows vulkan game:  
```
WINEARCH=win64 WINEPREFIX="/some/directory/path/DOOM2016" ./installvulkan.sh  
```
Game with dxvk:  
```
WINEARCH=win64 WINEPREFIX="/some/directory/path/tombraider" DXVK=1 ./installvulkan.sh  

```
Game with vulkansdk:  
```
WINEARCH=win64 WINEPREFIX="/some/directory/path/tombraider" VULKANSDK=1 ./installvulkan.sh  
```
Game with both dxvk and vulkansdk:  
```
WINEARCH=win64 WINEPREFIX="/some/directory/path/tombraider" VULKANSDK=1 DXVK=1 ./installvulkan.sh  
```
Then launch your game as you normally would using that prefix.  

**NOTE: Don't forget to add quotation marks on the wine prefix in case your path has spaces in it.  


# Optional dxvkbuild.sh - use this to compile dxvk and output it to a directory.  
First chmod the script:
```
chmod a+x dxvkbuild.sh
```
Usage:  
Place this script inside a freshly cloned dxvk directory, then use it like so:  
```
DXVKDIR="/path/to/folder/containing/installvulkan.sh" ./dxvkbuild.sh  
```
NOTE: If the DXVKDIR directory contains win64 and/or win32 folders, delete them before running the script.  

wine-vulkan source:  
[wine-vulkan](http://github.com/roderickc/wine-vulkan)  

dxvk source:  
[dxvk](https://github.com/doitsujin/dxvk)  
