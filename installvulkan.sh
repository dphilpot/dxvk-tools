#!/bin/bash

# kill wineserver before installation to make sure correct wine version is used
wineserver -k

WINECMD=${WINE-wine}

if [ $WINECMD = "wine" ]; then
    if [ $WINEARCH = "win64" ]; then
        WINECMD="wine64"
    else
        WINECMD="wine"
    fi
fi

if [ ! -z "$VULKANSDK" ]; then
    WINEARCH="$WINEARCH" WINEPREFIX="$WINEPREFIX" $WINECMD regedit /S vulkan.reg

    if [ ! -f VulkanSDK-1.0.51.0-Installer.exe ]; then
        wget https://sdk.lunarg.com/sdk/download/1.0.51.0/windows/VulkanSDK-1.0.51.0-Installer.exe?Human=true -O VulkanSDK-1.0.51.0-Installer.exe -O VulkanSDK-1.0.51.0-Installer.exe
    fi

    WINEARCH="$WINEARCH" WINEPREFIX="$WINEPREFIX" "$WINECMD" VulkanSDK-1.0.51.0-Installer.exe /S
    cp winevulkan.json "$WINEPREFIX"/drive_c/windows/winevulkan.json
    echo "Vulkan SDK installation completed."
fi

if [ ! -z "$DXVK" ]; then
    if [ ! -d "$WINEARCH" ]; then
        mkdir $WINEARCH
        URLPREFIX=${WINEARCH:3}
        curl https://haagch.frickel.club/files/dxvk/latest/"$URLPREFIX"/bin/dxgi.dll -o $WINEARCH/dxgi.dll 
        curl https://haagch.frickel.club/files/dxvk/latest/"$URLPREFIX"/bin/d3d11.dll -o $WINEARCH/d3d11.dll
        curl https://haagch.frickel.club/files/dxvk/latest/"$URLPREFIX"/bin/setup_dxvk.sh -o $WINEARCH/setup_dxvk.sh
        if [ $WINEARCH = "win64" ]; then
            if [ ! -d "win32" ]; then
                mkdir win32
                curl https://haagch.frickel.club/files/dxvk/latest/32/bin/dxgi.dll -o win32/dxgi.dll 
                curl https://haagch.frickel.club/files/dxvk/latest/32/bin/d3d11.dll -o win32/d3d11.dll
                curl https://haagch.frickel.club/files/dxvk/latest/32/bin/setup_dxvk.sh -o win32/setup_dxvk.sh
            fi
        fi
    fi
    
    WINEARCH=$WINEARCH WINEPREFIX="$WINEPREFIX" bash $WINEARCH/setup_dxvk.sh
    
    if [ $WINEARCH = "win64" ]; then
        WINEARCH=$WINEARCH WINEPREFIX="$WINEPREFIX" bash win32/setup_dxvk.sh
    fi
    echo "DXVK installation completed."
fi

if [ -z "$VULKANSDK" ] && [ -z "$DXVK" ]; then
    echo "Error: No options specified. Please specify VULKANSDK=1 and/or DXVK=1"
fi

wineserver -k
exit
