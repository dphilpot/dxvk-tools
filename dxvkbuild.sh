#!/bin/bash

if [ -z "$DXVKDIR" ]; then
    echo 'Please specify a destination directory using DXVKDIR="/some/path" ./dxvkbuild.sh'
    exit
fi


mkdir -p "$DXVKDIR/win64"
mkdir -p "$DXVKDIR/win32"

export DESTDIR="$DXVKDIR/win64"

meson --cross-file build-win64.txt build.w64
cd build.w64
meson configure -Dbuildtype=release
ninja
ninja install

mv "$DXVKDIR"/win64/usr/local/bin/dxgi.dll "$DXVKDIR"/win64/
mv "$DXVKDIR"/win64/usr/local/bin/d3d11.dll "$DXVKDIR"/win64/
mv "$DXVKDIR"/win64/usr/local/bin/d3d10_1.dll "$DXVKDIR"/win64/
mv "$DXVKDIR"/win64/usr/local/bin/d3d10.dll "$DXVKDIR"/win64/
mv "$DXVKDIR"/win64/usr/local/bin/d3d10core.dll "$DXVKDIR"/win64/
mv "$DXVKDIR"/win64/usr/local/bin/setup_dxvk.sh "$DXVKDIR"/win64/


cd ..

export DESTDIR="$DXVKDIR/win32"

meson --cross-file build-win32.txt build.w32
cd build.w32
meson configure -Dbuildtype=release
ninja
ninja install

mv "$DXVKDIR"/win32/usr/local/bin/dxgi.dll "$DXVKDIR"/win32/
mv "$DXVKDIR"/win32/usr/local/bin/d3d11.dll "$DXVKDIR"/win32/
mv "$DXVKDIR"/win32/usr/local/bin/d3d10_1.dll "$DXVKDIR"/win32/
mv "$DXVKDIR"/win32/usr/local/bin/d3d10.dll "$DXVKDIR"/win32/
mv "$DXVKDIR"/win32/usr/local/bin/d3d10core.dll "$DXVKDIR"/win32/
mv "$DXVKDIR"/win32/usr/local/bin/setup_dxvk.sh "$DXVKDIR"/win32/
